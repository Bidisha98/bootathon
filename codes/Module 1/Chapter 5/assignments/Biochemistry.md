### Objective:

Import and simulate the model from pantherdb and understand the functions of genes based on their evolutionary relationships using pantherdb in CellDesigner.


### Theory:

Protein Analysis through Evolutionary Relationships (PANTHER) is a comprehensive system for understanding the functions of genes from the ancestor to the modern genes based on their evolutionary relationships. Pantherdb gives information about Gene of interest. It searches for protein families, molecular functions, biological process and pathways. It generates the information about the genes that belong to a given protein family or sub family and analyze the lists of genes and proteins based on family, molecular function, biological process, cellular component or pathways. The main function of PANTHER is the accurate prediction of the functions of uncharacterized genes, based on their evolutionary relationships between genes. It also includes tools for analyzing genomic data which is relative to known and inferred gene functions. One can easily download the pathways from PANTHER in System Biology Markup Language (SBML) format, so it is easy to view the model using CellDesigner.

#### Important features of Pantherdb:

- Whole genome sequence coverage from 48 genomes (upto 87% of genes).
- New tree building algorithm (GIGA) for improved phylogenetic relationships of genes and families.
- Improved Ortholog identiﬁcation.
- Improved Hidden Markov Models.
- Expanded sets of genomes and sequence identiﬁer for PANTHER tools.
- PANTHER Pathway diagram represents in SBGN.


 Home page of `pantherdb.org` is represented as shown below :

 ![Bio](figss.png)
 

 ### Overview and functionalities of PATHER:


**Browse:** User can browse the data by functions, pathways and species in Prowler search.

**Genes and Orthologs:** Links to the Gene, Genome and Orthologs page where user can perform search by keyword, whole Genome PieCharts and upload batch IDs.

 

**Trees and HMMs:** Redirects to the page which links to functions related to trees and HMMs, such as HMM scoring.

 

**Pathways:** Redirects to the page that contains pathway related knowledge.

 

**Ontologies:** Redirects to the page that provides information about the PANTHER Gene Ontology Slim (GO slims are lists of GO terms that have been selected from the terms that are available from the Gene Ontology project) and PANTHER Protein Classes.

 

**Tools:** Redirects to all the PANTHER tools.

 

**Whole genome function views:** Redirects to the whole genome pie chart.

 

**Gene expression tools:** Redirected to the Gene Expression Analysis Tools.

 

**cSNP tools:** Redirects directly to the cSNP Scoring tool page.

 

**Upload multiple gene IDs:** Redirects directly to the Batch ID Upload page.

 

**Community Curation:** This page allows to download PAINT (Phylogenetic tree Annotation Inference Tool) application.

 

**My Workspace:** Redirects to the workspace available to the registered users.

 

**HMM scoring:** Redirects to the PANTHER HMM Scoring page.

 

**Downloads:** Redirects to the PANTHER FTP site.

 

**Genome statistics:** Redirects to page with statistics of 48 genomes in the PANTHER library

 

**Site map:** Redirects to the site map.

 

**Keyword search:** It performs simple keywords search for genes, proteins, families, ontology and pathways.

 

**Sequence search:** Enter a protein sequence to score against PANTHER HMM library.

 

**Publications:** This section lists some of the PANTHER publications and information about citing PANTHER.


### CellDesigner:
 

Systems biology deals with mainly modeling of biological systems at system level. The main purpose of CellDesigner is to draw biological networks using the symbolic notation, this system is proposed by Kitano. It is easy to retrieve the model from the databases.

 

#### Features of cell designer :

 - User can draw reaction easily
  It is easy to make a model or to build a network based on mathematical equations.

 

- The user can open a model from a pre-existing repository
Repository (database) allows one to store, search, and retrieve the mathematical models. These models are curated with the external resources like publications, ontologies, databases of compounds and pathways. 

 

- Data can be read and written in SBML format
Different format is needed for computing a model to the point where it can be simulated and analyzed. Systems Biology Markup Language (SBML) is used for analyzes.

 

- Simulate the reactions by changing the conditions and can view results graphically.
Select a particular model and run the simulation by changing the parameters. The results can be viewed graphically with the time on the X-axis and species (or substances) on the Y-axis. It implies that, what will be expression of particular substance at a particular time, based on an equation.

 
 
