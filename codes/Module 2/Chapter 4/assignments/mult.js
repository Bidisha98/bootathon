function mul() {
    var count = document.getElementById("n");
    var table = document.getElementById("table");
    var n = +(count.value); //typecasting to integer value
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    console.log(n);
    for (let i = 1; i <= n; i++) {
        var row = table.insertRow(); // row insertion
        var cella = row.insertCell();
        var cellb = row.insertCell();
        var cellc = row.insertCell(); // cell insertion
        cella.innerHTML = n.toString() + " * " + i.toString(); //assignment of value 
        cellb.innerHTML = " = ";
        cellc.innerHTML = (n * i).toString();
    }
}
//# sourceMappingURL=mult.js.map